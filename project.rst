.. _project:

Projekte verwalten
==================

Um Projekte zu verwalten, navigieren Sie im :ref:`Backend <login>` zu *Projects*.

Projekte werden direkt auf der Webseite dargestellt und können bei der :ref:`Partnerverwaltung <partner>` referenziert werden.

.. _new_projectcategory:

Projektkategorie
----------------
Um Projektkategorien zu verwalten,  navigieren Sie im :ref:`Backend <login>` zu *Projects/Categories*

Hinzufügen
~~~~~~~~~~
1. Füllen Sie hier das Formular auf der linken Seite aus und klicken anschließend auf
2. *Add New Category*

.. image:: _static/images/project/new_category.png

Die neue Projektkategorie erscheint nun auf der rechten Seite.

Projekt
-------

Hinzufügen
~~~~~~~~~~

Um Projekte hinzuzufügen, navigieren Sie im :ref:`Backend <login>` zu *Projects/Add New*.

Füllen Sie zunächst die gewohnten Felder *Titel* und *Inhalt* aus. Achten Sie hierbei darauf, den Text nicht zu lange zu gestalten, da sonst die Lesbarkeit auf der Webseite eingeschränkt wird. Wenige Absätze sind zu empfehlen.

.. image:: _static/images/project/new_main.png

Füllen Sie anschließend die Box *Dates* aus. Diese Information wird später vor dem Projekttiel dargestellt und dient der Sortierung.

.. image:: _static/images/project/dates.png

Weisen Sie dem Projekt außerdem (falls vorhanden) ein :ref:`Bild <media_choose_image>` und eine :ref:`Kategorie <new_projectcategory>` zu.

.. image:: _static/images/project/logo_category.png

Außerdem kann an dieser Stelle ein Projektkoordinator und Projektpartner ausgewählt werden. Dazu müssen diese zunächst als :ref:`Partner <partner>` angelegt werden.

.. image:: _static/images/project/partner.png

Löschen
~~~~~~~

ToDo
