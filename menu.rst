.. _menu:

Menü editieren
==============

Um das Menü zu editieren, navigieren Sie im :ref:`Backend <login>` zu *Design/Menüs*.

Wählen Sie nun aus der linken Spalte Inhalte aus, die Sie zum Menü hinzufügen möchten. Klicken Sie dazu auf den enstsprechenden Inhaltstyp (zB Seiten) und setzen Haken für die hinzuzufügenden Elemente.

.. image:: _static/images/menu/add.png

Durch einen Klick auf *Zum Menü hinzufügen* erscheinen die neuen Menüpunkte auf der rechten Seite. Hier können die Seiten mittels *Drag&Drop angeordnet werden*. Verschieben Sie die Seite horizontal, um Untermenüs zu erzeugen. Klicken Sie anschließend auf *Menü speichern*.

.. image:: _static/images/menu/order.png
