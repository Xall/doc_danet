.. _login:

Die Admin Ansicht öffnen
========================

Um die Admin Ansicht (im folgenden "Backend" genannt) muss folgende URL eingegeben werden:

`<http://www.danube-networkers.eu/admin>`_

.. image:: _static/images/login/prompt.png

Geben Sie hier Ihren 

1. Benutzernamen

und 

2. Passwort

ein.

Anschließend wählen Sie die zu bearbeitende Seite über die Schaltfläche 

1. *Meine Websites*

aus.

.. image:: _static/images/login/sites.png
