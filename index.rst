.. danet documentation master file, created by
   sphinx-quickstart on Wed Mar 30 19:04:59 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Danube Networkers Dokumentations
=================================

Inhalt:

.. toctree::
   :maxdepth: 2
   
   login
   menu
   partner
   project
   newsletter
   media


              

Referenzen
==========


* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

